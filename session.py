# Copyright (C) 2015, see AUTHOR for contributors
#
# This file is part of CyberTimer.
#
# CyberTimer is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CyberTimer is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with CyberTimer.  If not, see <http://www.gnu.org/licenses/>.

import threading
import json

class Session:

	RUNNING = "running"
	PAUSED = "paused"
	IDLE = "idle"
	STARTING = "starting"
	PAUSING = "pausing"
	STOPPING = "stopping"
	SERMODE_FULL = "full"
	SERMODE_CLIENT = "client"

	def __init__(self, address, name):
		self.name = name
		self.tag = ""
		self.remainingTime = 0
		self.elapsedTime = 0
		self.state = Session.IDLE
		self.address = address
		self.unavailable = True
		self.ascending = False
		self.locking = True
		self.listeners = []
		self.timer = None

	def addTime(self, time):
		self.remainingTime += time

	def reqStartSession(self):
		if self.state == Session.IDLE \
		or self.state == Session.PAUSED:
			if self.remainingTime == 0 and not self.ascending:
				self.ascending = True
			self.state = Session.STARTING
	def startSession(self):
		if self.state == Session.STARTING:
			self.state = Session.RUNNING
			self._startTimer()

	def reqPauseSession(self):
		if self.state == Session.RUNNING:
			self.state = Session.PAUSING
	def pauseSession(self):
		if self.state == Session.PAUSING:
			self.state = Session.PAUSED
			self._stopTimer()

	def reqStopSession(self):
		if self.unavailable:
			# Kill the session if unavailable
			self.stopSession()
		if self.state == Session.RUNNING \
		or self.state == Session.PAUSED:
			# Ask for shutdown
			self.state = Session.STOPPING

	def stopSession(self):
		self.tag = ""
		self.remainingTime = 0
		self.elapsedTime = 0
		self.ascending = False
		self.state = Session.IDLE
		self._stopTimer()

	def _startTimer(self):
		if self.timer:
			self.timer.cancel()
		self.timer = threading.Timer(1.0, self._tick)
		self.timer.start()
	def _stopTimer(self):
		if self.timer:
			self.timer.cancel()
			self.timer = None

	def _tick(self):
		if self.state == Session.RUNNING:
			if self.ascending:
				self.elapsedTime += 1
			else:
				self.remainingTime -= 1
			self._startTimer()
		if self.remainingTime <= 0 and not self.ascending:
			self.stopSession()
		self.notifyListeners()

	def serialize(self, mode=None):
		if mode == None:
			mode = Session.SERMODE_FULL
		ser = {
			"name": self.name,
			"state": self.state,
			"unavailable": self.unavailable,
			"address": self.address,
			"locking": self.locking,
		}
		if self.ascending:
			ser['elapsedTime'] = self.elapsedTime
		else:
			ser['remainingTime'] = self.remainingTime
		if mode == Session.SERMODE_FULL:
			ser['tag'] = self.tag
		return ser
	@staticmethod
	def deserialize(data):
		session = Session(data['address'], data['name'])
		if "tag" in data:
			session.tag = data['tag']
		if "remainingTime" in data:
			session.remainingTime = data['remainingTime']
			session.ascending = False
		else:
			session.elapsedTime = data['elapsedTime']
			session.ascending = True
		session.state = data['state']
		session.locking = data['locking']
		if session.state == Session.RUNNING:
			session._startTimer()
		return session
	
	def addListener(self, listener):
		self.listeners.append(listener)

	def removeListener(self, listener):
		if listener in self.listeners:
			self.listeners.remove(listener)

	def notifyListeners(self):
		for listener in self.listeners:
			listener.sessionUpdated(self)

class SessionList:
	"""Session array for serialization"""
	# TODO: this is an useless class for ctx.py to avoid a nice code

	def __init__(self, sessions):
		self.sessions = sessions

	def serialize(self, unused): # unused, but required for ugly code
		ser = []
		for session in self.sessions:
			ser.append(session.serialize(Session.SERMODE_CLIENT))
		return ser