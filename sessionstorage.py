# Copyright (C) 2015, see AUTHOR for contributors
#
# This file is part of CyberTimer.
#
# CyberTimer is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CyberTimer is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with CyberTimer.  If not, see <http://www.gnu.org/licenses/>.

fileLock = False
import json
import logging
from session import Session

def init(filename):
	"""Load the session configuration"""
	f = open(filename, 'rb')
	sessions = []
	line = f.readline()
	while line:
		linedata = line.split(";")
		if len(linedata) != 2:
			logging.warning("Ignored invalid client data: %s"%line)
			line = f.readline()
			continue
		address = linedata[0]
		name = linedata[1]
		if name[-1] == '\n':
			if name[-2] == '\r':
				name = linedata[1][:-2]
			else:
				name = linedata[1][:-1]
		sessions.append({'address': address, 'name': name})
		line = f.readline()
	f.close
	return sessions

def read(filename):
	"""Load sessions from a storage file"""
	global fileLock
	if fileLock:
		raise SaveLockError("File is locked")
	try:
		f = open(filename, 'rb')
		rawdata = f.read()
		f.close()
		if rawdata:
			data = json.loads(rawdata)
			ret = []
			for unser in data:
				ret.append(Session.deserialize(unser))
			return ret
		else:
			return []
	except IOError:
		return []

def write(sessions, filename):
	"""Save sessions to a storage file"""
	global fileLock
	serialized = []
	for session in sessions:
		# Don't store idle and closed sessions
		if not (session.state == Session.IDLE and session.remainingTime == 0):
			serialized.append(session.serialize())
	if fileLock:
		raise SaveLockError("File is locked")
	fileLock = True
	f = open(filename, 'wb')
	f.write(json.dumps(serialized))
	f.close()
	fileLock = False

class SaveLockError(Exception):

	def __init__(self, message):
		Exception.__init__(self, message)
