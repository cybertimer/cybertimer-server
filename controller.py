# Copyright (C) 2015, see AUTHOR for contributors
#
# This file is part of CyberTimer.
#
# CyberTimer is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CyberTimer is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with CyberTimer.  If not, see <http://www.gnu.org/licenses/>.

from session import Session
from session import SessionList
import time
import threading
import logging

sessionAddr = {}
sessionAddrList = []
lastMessageAddr = {}

def _updateLastMessage(host):
	lastMessageAddr[host] = int(time.time())

def addSession(session):
	sessionAddr[session.address] = session
	sessionAddrList.append(session.address)

def sessionList():
	ret = []
	for addr in sessionAddrList:
		ret.append(sessionAddr[addr])
	return ret

def cmdList(host, args):
	"""List command to list monitored stations and their states.
	Authorised only from kiosk clients."""
	# TODO: reject command from unauthorised client
	#if host not in kioskAddr:
	#	return None
	return SessionList(sessionList())

def cmdSession(host, args):
	"""Connection command to discover active sessions"""
	if host not in sessionAddr:
		return None
	_updateLastMessage(host)
	session = sessionAddr[host]
	if session.unavailable:
		# The session is now identified
		logging.debug("%s is now available"%session.name)
		session.unavailable = False
		session.notifyListeners()
	return session

def cmdStarted(host, args):
	"""Acknowledgement command for starting"""
	if host not in sessionAddr:
		return None
	_updateLastMessage(host)
	session = sessionAddr[host]
	session.startSession()
	session.notifyListeners()
	logging.debug("%s started"%session.name)
	return session

def cmdPaused(host, args):
	"""Acknowledgement command for pausing"""
	if host not in sessionAddr:
		return None
	_updateLastMessage(host)
	session = sessionAddr[host]
	session.pauseSession()
	session.notifyListeners()
	logging.debug("%s paused"%session.name)
	return session

def cmdStop(host, args):
	"""Acknowledgement command for stopping"""
	if host not in sessionAddr:
		return None
	_updateLastMessage(host)
	session = sessionAddr[host]
	session.stopSession()
	session.notifyListeners()
	logging.debug("%s stopped"%session.name)
	return session
def addTime(session, time):
	session.addTime(time)

def cmdCredit(host, args):
	# TODO: check from where the command comes to allow/reject it
	# TODO: reject command from unauthorised client
	#if host not in kioskAddr:
	#	return None
	if 'address' not in args:
		return None
	address = args['address']
	if address not in sessionAddr:
		return None
	session = sessionAddr[address]
	if session.unavailable:
		return None
	session.addTime(20 * 60) # TODO pulses in config
	session.reqStartSession
	session.startSession()
	session.notifyListeners()
	return session

IDLE_TIME = 30
def isSessionActive(host):
	"""Check if a session is active (received a message
	whithin X seconds"""
	if host not in lastMessageAddr:
		return False
	now = int(time.time())
	return (now - lastMessageAddr[host] < IDLE_TIME)

class Watcher:
	"""Watcher to detect disconnected sessions"""

	def __init__(self):
		global sessionAddr
		self.hosts = sessionAddr
		self.updateTimer = None
		self.killed = False
		self._startWatching()

	def _startWatching(self):
		if self.updateTimer:
			self.updateTimer.cancel()
		if not self.killed:
			self.updateTimer = threading.Timer(5.0, self._updateTmr)
			self.updateTimer.start()

	def stop(self):
		if self.updateTimer:
			self.updateTimer.cancel()
			self.updateTtimer = None
			self.killed = True

	def _updateTmr(self):
		self._watch()
		self._startWatching()

	def _watch(self):
		global sessionAddr
		for host in self.hosts:
			if host in sessionAddr:
				session = sessionAddr[host]
				if not session.unavailable and not isSessionActive(host):
					session.unavailable = True
					session.notifyListeners()
