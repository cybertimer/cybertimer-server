# Copyright (C) 2015, see AUTHOR for contributors
#
# This file is part of CyberTimer.
#
# CyberTimer is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CyberTimer is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with CyberTimer.  If not, see <http://www.gnu.org/licenses/>.

import Tkinter
import socket
import sessionstorage
import controller
import gettext
_ = gettext.gettext
gettext.bindtextdomain("cybertimer", "./i18n")
gettext.textdomain("cybertimer")
import ConfigParser
import logging
from ctx import ListenThread
from session import Session
from mainscreen import MainScreen

VERSION = "0.1"

cfg = {'port': 9234,
	'clientfile': "./clients.txt",
	'backupfile': "./state.dat"}

def main():
	# Load config
	parser = ConfigParser.ConfigParser()
	parser.read("config.ini")
	if parser.has_option("server", "port"):
		cfg['port'] = parser.getint("server", "port")
	if parser.has_option("server", "clientfile"):
		cfg['clientfile'] = parser.get("server", "clientfile")
	if parser.has_option("server", "backupfile"):
		cfg['backupfile'] = parser.get("server", "backupfile")
	# Configure logger
	logLevel = logging.WARNING
	logFile = None
	if parser.has_option("logging", "level"):
		logCfg = parser.get("logging", "level").lower()
		if logCfg == "debug":
			logLevel = logging.DEBUG
		elif logCfg == "info":
			logLevel = logging.INFO
		elif logCfg == "warn" or logCfg == "warning":
			logLevel = logging.WARNING
		elif logCfg == "err" or logCfg == "error":
			logLevel = logging.ERROR
		elif logCfg == "critical":
			logLevel = logging.CRITICAL
	if parser.has_option("logging", "file"):
		logFile = parser.get("logging", "file")
	logFormat = "%(asctime)-15s %(levelname)s %(message)s"
	logging.basicConfig(level=logLevel, filename=logFile, format=logFormat)
	# Init sessions from config
	sessionInit = sessionstorage.init(cfg['clientfile'])
	for s in sessionInit:
		session = Session(s['address'], s['name'])
		controller.addSession(session)
		logging.debug("Binding %s to %s"%(session.address, session.name))
	# Try restoring latest state
	storedSessions = sessionstorage.read(cfg['backupfile'])
	for restore in storedSessions:
		if controller.sessionAddr[restore.address]:
			controller.sessionAddr[restore.address] = restore
	# Init server
	loop = ListenThread(cfg['port'])
	loop.addCommand("session", controller.cmdSession)
	loop.addCommand("start", controller.cmdStarted)
	loop.addCommand("pause", controller.cmdPaused)
	loop.addCommand("stop", controller.cmdStop)
	loop.addCommand("list", controller.cmdList)
	loop.addCommand("credit", controller.cmdCredit)
	loop.start()
	# Start watcher
	watcher = controller.Watcher()
	try:
		# Init GUI
		screen = MainScreen()
		for s in controller.sessionList():
			screen.addSession(s)
		screen.mainloop()
	finally:
		logging.debug("Stopping server")
		loop.stop()
		watcher.stop()
		sessionstorage.write(controller.sessionList(), cfg['backupfile'])
		logging.debug("End")
main()
