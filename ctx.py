# Copyright (C) 2015, see AUTHOR for contributors
#
# This file is part of CyberTimer.
#
# CyberTimer is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CyberTimer is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with CyberTimer.  If not, see <http://www.gnu.org/licenses/>.

import threading
import json
import socket
import logging
from session import Session

class ListenThread(threading.Thread):

	def __init__(self, port):
		threading.Thread.__init__(self)
		self.srv = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.srv.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		self.srv.bind(('', port))
		self.srv.listen(20)
		logging.debug("Listening on port %d"%port)
		self.kill = False
		self.daemon = True
		self.commands = {}

	def addCommand(self, command, action):
		self.commands[command] = action

	def stop(self):
		self.kill = True
		self.srv.shutdown(socket.SHUT_RDWR)
		self.srv.close()
		logging.debug("Server socket closed")

	def run(self):
		while not self.kill:
			try:
				clientsocket, address = self.srv.accept()
				try:
					host = address[0]
					cmd = clientsocket.recv(1024)
					args = {}
					httpData = self._httpFilter(cmd)
					http = False
					if 'cmd' in httpData:
						cmd = httpData['cmd']
						if 'args' in httpData:
							args = httpData['args']
						http = True
					else:
						cmd, args = self._parseArgs(cmd)
					logging.debug("Command %s received from %s"%(cmd,address[0]))
					if cmd in self.commands:
						ret = self.commands[cmd](host, args)
						if ret != None:
							resp = json.dumps(ret.serialize(Session.SERMODE_CLIENT))
							if http:
								clientsocket.sendall(self._httpResponse(httpData['origin'], resp))
							else:
								clientsocket.sendall(resp)
				except:
					logging.warning("Socket error")
					logging.debug("Error while listening to calls", exc_info=True)
				finally:
					clientsocket.shutdown(socket.SHUT_RDWR)
					clientsocket.close()
			except:
				logging.warning("Socket error")
				logging.debug("Unable to create socket", exc_info=True)
		logging.debug("ListenThread killed")

	def _parseArgs(self, data):
		"""Parse arguments from command cmd arg=val arg=val"""
		args = {}
		couples = data.split(" ")[1:]
		cmd = data.split(" ")[0]
		for couple in couples:
			split = couple.split("=")
			if split.length == 2:
				args[split[0]] = split[1]
			else:
				warn("Unable to pass arguments for command %s. Arguments ignored."%data)
				return {}
		return (cmd, args)

	def _httpFilter(self, data):
		"""Simple filter to retrieve a command from an HTTP request.
		Returns the command if found or the untouched data otherwise."""
		if data[:3] != "GET":
			return data
		httpData = {'origin': None}
		lines = data.split("\n")
		for line in lines:
			if line[:3].upper() == "GET":
				parts = line.split(" ")
				getArgs = parts[1]
				params = getArgs[getArgs.find("?") + 1:].split("&")
				for param in params:
					args = param.split("=")
					arg = args[0]
					value = args[1]
					if arg == "cmd":
						httpData['cmd'] = value
					else:
						if 'args' not in httpData:
							httpData['args'] = {}
						httpData['args'][arg] = value
			if line[:6].lower() == "origin":
				httpData['origin'] = line[8:]
		return httpData
	def _httpResponse(self, origin, data):
		"""Format the answer with an http header"""
		return "HTTP/1.1 200 OK\nContent-Length: %d\nAccess-Control-Allow-Origin: %s\n\n%s"%(len(data), origin, data)