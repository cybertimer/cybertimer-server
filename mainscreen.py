# Copyright (C) 2015, see AUTHOR for contributors
#
# This file is part of CyberTimer.
#
# CyberTimer is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CyberTimer is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with CyberTimer.  If not, see <http://www.gnu.org/licenses/>.

import Tkinter
import gettext
_ = gettext.gettext
from session import Session

def tabFocus(event):
	event.widget.tk_focusNext().focus()
	return("break")

class SessionLine:

	def __init__(self, tkHost, session):
		self.unavailableImg = Tkinter.PhotoImage(file="res/state-unavailable.gif")
		self.availableImg = Tkinter.PhotoImage(file="res/state-available.gif")
		self.occupiedImg = Tkinter.PhotoImage(file="res/state-occupied.gif")
		self.pausedImg = Tkinter.PhotoImage(file="res/state-paused.gif")
		self.session = session
		self.cnt = Tkinter.Frame(tkHost)
		self.tag = Tkinter.Entry(self.cnt, width=10, justify=Tkinter.CENTER,
			validate="focusout", validatecommand=self.tagValidate)
		self.tag.pack(side=Tkinter.LEFT)
		self.name = Tkinter.Label(self.cnt, text=session.name, width=12)
		self.name.pack(side=Tkinter.LEFT)
		self.state = Tkinter.Label(self.cnt, image=self.unavailableImg)
		self.state.pack(side=Tkinter.LEFT)
		self.playImg = Tkinter.PhotoImage(file="res/media-playback-start.gif")
		self.play = Tkinter.Button(self.cnt, state=Tkinter.DISABLED, image=self.playImg,
			command=self.playClb)
		self.play.pack(side=Tkinter.LEFT)
		self.pauseImg = Tkinter.PhotoImage(file="res/media-playback-pause.gif")
		self.pause = Tkinter.Button(self.cnt, state=Tkinter.DISABLED, image=self.pauseImg,
			command=self.pauseClb)
		self.pause.pack(side=Tkinter.LEFT)
		self.stopImg = Tkinter.PhotoImage(file="res/media-playback-stop.gif")
		self.stop = Tkinter.Button(self.cnt, state=Tkinter.DISABLED, image=self.stopImg,
			command=self.stopClb)
		self.stop.pack(side=Tkinter.LEFT)
		self.time = Tkinter.Label(self.cnt, text="00:00", width=7)
		self.time.pack(side=Tkinter.LEFT)
		self.oneHour = Tkinter.Button(self.cnt, text="+1h",
			command=self.oneHourClb)
		self.oneHour.pack(side=Tkinter.LEFT)
		self.plus = Tkinter.Button(self.cnt, text="+",
			command=self.plusClb)
		self.plus.pack(side=Tkinter.LEFT)
		self.lockOffImg = Tkinter.PhotoImage(file="res/locked_off.gif")
		self.lockOnImg = Tkinter.PhotoImage(file="res/locked_on.gif")
		self.locked = Tkinter.Button(self.cnt, image=self.lockOnImg,
			command=self.lockSwitchClb)
		self.locked.pack(side=Tkinter.LEFT)
		self.cnt.pack()
		self.update()
		self.timePopup = None

	def update(self):
		try:
			if self.cnt.focus_get() != self.tag:
				# Don't update tag when focused (being edited)
				self.tag.delete(0, Tkinter.END)
				self.tag.insert(0, self.session.tag)
		except RuntimeError:
			# Screen is probably killed
			return
		if self.session.unavailable:
			self.play.config(state=Tkinter.DISABLED)
			self.pause.config(state=Tkinter.DISABLED)
			if self.session.remainingTime > 0 \
			or self.session.elapsedTime > 0:
				self.stop.config(state=Tkinter.NORMAL)
			else:
				self.stop.config(state=Tkinter.DISABLED)
			self.state.config(image=self.unavailableImg)
		else:
			self.state.config(text=self.session.state)
			if self.session.state == Session.RUNNING:
				self.play.config(state=Tkinter.DISABLED)
				self.pause.config(state=Tkinter.NORMAL)
				self.stop.config(state=Tkinter.NORMAL)
				self.state.config(image=self.occupiedImg)
			elif self.session.state == Session.IDLE:
				self.play.config(state=Tkinter.NORMAL)
				self.pause.config(state=Tkinter.DISABLED)
				self.stop.config(state=Tkinter.DISABLED)
				self.state.config(image=self.availableImg)
			elif self.session.state == Session.PAUSED:
				self.play.config(state=Tkinter.NORMAL)
				self.pause.config(state=Tkinter.DISABLED)
				self.stop.config(state=Tkinter.NORMAL)
				self.state.config(image=self.pausedImg)
			elif self.session.state == Session.STARTING \
			or self.session.state == Session.PAUSING \
			or self.session.state == Session.STOPPING:
				self.play.config(state=Tkinter.DISABLED)
				self.pause.config(state=Tkinter.DISABLED)
				self.stop.config(state=Tkinter.DISABLED)
				self.state.config(image=self.occupiedImg)
		if self.session.locking:
			self.locked.config(image=self.lockOnImg)
		else:
			self.locked.config(image=self.lockOffImg)
		if not self.session.ascending:
			minutes = (self.session.remainingTime / 60) % 60
			hours = self.session.remainingTime / 3600
			secs = self.session.remainingTime % 60
		else:
			minutes = (self.session.elapsedTime / 60) % 60
			hours = self.session.elapsedTime / 3600
			secs = self.session.elapsedTime % 60
		if secs > 3:
			minutes += 1
			if minutes == 60:
				minutes = 0
				hours += 1
		self.time.config(text="%02d:%02d"%(hours, minutes))
		if self.session.ascending:
			self.time.config(fg="#007700")
		else:
			if self.session.remainingTime <= 10 * 60 \
			and self.session.remainingTime > 0:
				self.time.config(fg="#cc0000")
			elif self.session.remainingTime > 0 :
				self.time.config(fg="#440000")
			else:
				self.time.config(fg="#000000")

	def tagValidate(self):
		self.session.tag = self.tag.get()
		return True

	def playClb(self):
		self.session.reqStartSession()
		self.session.notifyListeners()

	def pauseClb(self):
		self.session.reqPauseSession()
		self.session.notifyListeners()

	def stopClb(self):
		self.session.reqStopSession()
		self.session.notifyListeners()

	def oneHourClb(self):
		self.session.addTime(3600)
		if self.session.remainingTime == 3600:
			self.session.reqStartSession()
		self.session.notifyListeners()
		pass

	def plusClb(self):
		if self.timePopup:
			try:
				self.timePopup.destroy()
			except Tkinter.TclError:
				# Popup is already dead
				pass
		self.timePopup = Tkinter.Tk()
		self.timePopup.wm_title(_("Add time to %s")%(self.session.name))
		promptLbl = Tkinter.Label(self.timePopup,
			text=_("Enter time to add to %s")%(self.session.name))
		promptLbl.pack(side=Tkinter.TOP)
		timeCnt = Tkinter.Frame(self.timePopup)
		self.addHourTxt = Tkinter.Entry(timeCnt, width=3)
		self.addHourTxt.bind("<Tab>", tabFocus)
		self.addHourTxt.pack(side=Tkinter.LEFT)
		addSepLbl = Tkinter.Label(timeCnt, text=":", width=2)
		addSepLbl.pack(side=Tkinter.LEFT)
		self.addMinTxt = Tkinter.Entry(timeCnt, width=3)
		self.addMinTxt.bind("<Tab>", tabFocus)
		self.addMinTxt.pack(side=Tkinter.LEFT)
		timeCnt.pack(side=Tkinter.TOP)
		btnCnt = Tkinter.Frame(self.timePopup)
		okBtn = Tkinter.Button(btnCnt, text=_("Ok"), command=self.plusOkClb)
		cancelBtn = Tkinter.Button(self.timePopup, text=_("Cancel"),
			command=self.timePopup.destroy)
		cancelBtn.pack(side=Tkinter.RIGHT)
		okBtn.pack(side=Tkinter.RIGHT)
		btnCnt.pack(side=Tkinter.TOP)

	def plusOkClb(self):
		try:
			hours = int(self.addHourTxt.get())
		except ValueError:
			hours = 0
		try:
			mins = int(self.addMinTxt.get())
		except ValueError:
			mins = 0
		if hours != 0 or mins != 0:
			mins *= 60
			mins += hours * 3600
			self.session.addTime(mins)
			self.session.notifyListeners()
		self.timePopup.destroy()

	def lockSwitchClb(self):
		self.session.locking = not self.session.locking
		self.session.notifyListeners()

class MainScreen(Tkinter.Tk):

	def __init__(self):
		Tkinter.Tk.__init__(self)
		self.protocol("WM_DELETE_WINDOW", self._close)
		self.wm_title(_("Cybertimer"))
		self.cnt = Tkinter.Frame(self)
		self.cnt.pack()
		self.lines = []

	def addSession(self, session):
		line = SessionLine(self.cnt, session)
		self.lines.append(line)
		session.addListener(self)

	def update(self, session = None):
		if session:
			for line in self.lines:
				if line.session.address == session.address:
					line.update()
		else:
			for line in self.lines:
				line.update()

	def sessionUpdated(self, session):
		self.update(session)

	def _close(self):
		for line in self.lines:
			line.session.removeListener(self)
		self.destroy()